<div style="margin-top:45px;"></div>
<div class='clear'>
  <small class="muted">
    <?php echo sprintf(t('%sShareaholic for Drupal v'. ShareaholicUtilities::get_version() .'%s | %sPrivacy Policy%s | %sTerms of Service%s | %sSupport Center%s | %sAPI%s | %sSocial Analytics%s'), '<a href="https://www.shareaholic.com/?src=drupal_admin" target="_new">', '</a>', '<a href="https://www.shareaholic.com/privacy/?src=drupal_admin" target="_new">', '</a>', '<a href="https://www.shareaholic.com/terms/?src=drupal_admin" target="_new">', '</a>', '<a href="https://support.shareaholic.com/" target="_new">', '</a>', '<a href="https://shareaholic.com/api/?src=drupal_admin" target="_new">', '</a>', '<a href="https://www.shareaholic.com/website-tools/content-analytics/?site='. ShareaholicUtilities::get_host() .'&src=drupal_admin" target="_new">', '</a>'); ?>
  </small>
  <br />
  <small class="muted">
    <?php echo sprintf(t('%sGet Involved%s. Made with much love in Boston, Massachusetts.'), '<a href="https://github.com/shareaholic/shareaholic_for_drupal" target="_new">', '</a>'); ?>
  </small>
  <br />
  <br />
  <iframe src="//www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fshareaholic&amp;width&amp;layout=standard&amp;action=like&amp;show_faces=true&amp;share=false&amp;height=80&amp;width=500&amp;appId=207766518608" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:500px; height:80px;" allowTransparency="true"></iframe>
</div>

<!-- Start of Async HubSpot Analytics -->
<script type="text/javascript">
var _hsq = _hsq || [];
_hsq.push(["setContentType", "standard-page"]);
	(function(d,s,i,r) {
	if (d.getElementById(i)){return;}
	var n = d.createElement(s),e = document.getElementsByTagName(s)[0];
	n.id=i;n.src = '//js.hubspot.com/analytics/'+(Math.ceil(new Date()/r)*r)+'/210895.js';
	e.parentNode.insertBefore(n, e);
	})(document, "script", "hs-analytics",300000);
</script>
<!-- End of Async HubSpot Analytics Code -->
  
<script src="https://dsms0mj1bbhn4.cloudfront.net/assets/pub/loader-reachable.js" async></script>
  